package main

import (
	"flag"
	"goShort/internal"
)

func main() {
	configPath := flag.String("config", "./config.yml", "path to the config file")
	flag.Parse()
	internal.LoadConfig(*configPath)
	internal.CreateHashIfNotExisting()
	internal.Connect()
	internal.StartServer()
}
