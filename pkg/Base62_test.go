package pkg

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestMinus(t *testing.T) {
	random, err := Random(-1)
	assert.Equal(t, "", random, "Should return empty String")
	assert.Nil(t, err, "Should return no error")
}

func TestNull(t *testing.T) {
	random, err := Random(0)
	assert.Equal(t, "", random, "Should return empty String")
	assert.Nil(t, err, "Should return no error")
}

func TestNormal(t *testing.T) {
	random, err := Random(11)
	assert.Equal(t, 11, len(random), "Should return String of size 11")
	assert.Nil(t, err, "Should return no error")
}

func BenchmarkRandom2(b *testing.B) {
	benchmarkBase62(2, b)
}
func BenchmarkRandom5(b *testing.B) {
	benchmarkBase62(5, b)
}
func BenchmarkRandom10(b *testing.B) {
	benchmarkBase62(10, b)
}
func BenchmarkRandom50(b *testing.B) {
	benchmarkBase62(50, b)
}
func BenchmarkRandom100(b *testing.B) {
	benchmarkBase62(100, b)
}
func benchmarkBase62(i int, b *testing.B) {
	for n := 0; n < b.N; n++ {
		_, _ = Random(i)
	}

}
