package pkg

import (
	"crypto/rand"
	"fmt"
)

const charset = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"
const csLen = byte(len(charset))

// Random generates a Random string using base-62 characters.
func Random(length int) (string, error) {
	if length <= 0 {
		return "", nil
	}
	output := make([]byte, 0, length)

	// Request a bit more than length to reduce the chance
	// of needing more than one batch of Random bytes
	batchSize := length + length/4

	for {
		buf, err := GenerateRandomBytes(batchSize)
		if err != nil {
			return "", err
		}

		for _, b := range buf {
			// Avoid bias by using a value range that's a multiple of 62
			if b < (csLen * 4) {
				output = append(output, charset[b%csLen])

				if len(output) == length {
					return string(output), nil
				}
			}
		}
	}
}

// GenerateRandomBytes is used to generate Random bytes of given size.
func GenerateRandomBytes(size int) ([]byte, error) {
	buf := make([]byte, size)
	if _, err := rand.Read(buf); err != nil {
		return nil, fmt.Errorf("failed to read Random bytes: %v", err)
	}
	return buf, nil
}
