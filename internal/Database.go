package internal

import (
	"fmt"
	log "github.com/sirupsen/logrus"
	"goShort/pkg"
	"gorm.io/driver/postgres"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

type RedirectUrl struct {
	gorm.Model
	Random      string
	Destination string
}

var database *gorm.DB

const POSTGRESQL = "postgresql"
const SQLITE = "sqlite"

func Connect() {
	var err error
	if configuration.databaseType == POSTGRESQL {
		database, err = ConnectPostgresSql()
	} else {
		database, err = ConnectFileBased()
	}

	if err != nil {
		errString := fmt.Sprintf("failed to connect database. %v", err)
		log.Error(errString)
		panic(errString)
	}
	// Migrate the schema
	err = database.AutoMigrate(&RedirectUrl{})

	if err != nil {
		errString := fmt.Sprintf("Could not Migrate Datamodel. %v", err)
		log.Error(errString)
		panic(errString)
	}
}

func ConnectPostgresSql() (db *gorm.DB, err error) {
	dsn := fmt.Sprintf("host=%v user=%v password=%v dbname=%v port=%v sslmode=disable",
		configuration.databaseHost, configuration.databaseUsername, configuration.databasePassword, configuration.databaseDatabase,
		configuration.databasePort)
	return gorm.Open(postgres.Open(dsn), &gorm.Config{})
}

func ConnectFileBased() (db *gorm.DB, err error) {
	return gorm.Open(sqlite.Open(configuration.databaseHost), &gorm.Config{
		SkipDefaultTransaction: true,
	})
}

func findRedirectUrl(random string) RedirectUrl {
	var redirectUrl RedirectUrl
	database.First(&redirectUrl, "Random = ?", random)
	return redirectUrl
}

func findRandom(url string) RedirectUrl {
	var redirectUrl RedirectUrl
	database.First(&redirectUrl, "Destination = ?", url)

	return redirectUrl
}

func addUrl(url string) string {
	var redirectUrl RedirectUrl

	tx := database.Begin()
	redirectUrl = findRandom(url)
	if redirectUrl.ID > 0 {
		log.Debug(fmt.Sprintf("Found something \"%v\"", redirectUrl))
		return redirectUrl.Random
	}

	random, err := pkg.Random(configuration.suffixLength)
	if err != nil {
		errString := fmt.Sprintf("Could not generate UUID. %v", err)
		log.Error(errString)
		panic(errString)
	}
	redirectUrl = RedirectUrl{Random: random, Destination: url}
	tx.Save(&redirectUrl)
	tx.Commit()
	log.Debug(fmt.Sprintf("Added Url \"%v\" with code %v", url, random))
	return random
}

func getAll() []RedirectUrl {
	tx := database.Begin()
	var redirectUrls []RedirectUrl
	result := tx.Find(&redirectUrls)
	if result.Error != nil {
		errString := fmt.Sprintf("Could not get Result. %v\n", result.Error)
		log.Error(errString)
		panic(errString)
	}

	tx.Commit()
	return redirectUrls
}
