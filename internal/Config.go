package internal

import (
	"fmt"
	argonpass "github.com/dwin/goArgonPass"
	"github.com/gookit/config"
	"github.com/gookit/config/yaml"
	log "github.com/sirupsen/logrus"
	"os"
)

type Configuration struct {
	port             int
	databaseType     string // Value from POSTGRESQL or SQLITE
	databaseHost     string
	databaseDatabase string
	databaseUsername string
	databasePassword string
	databasePort     int
	urlPrefix        string
	defaultResponse  string
	suffixLength     int
	username         string
	debug            bool
}

var configuration Configuration
var fileLocation = "config.yml"

const configReadErrorMessage = "Configuration File Reading Error for key %v"

var argonParams = argonpass.ArgonParams{
	Time:        argonpass.DefaultTime * 2,
	Memory:      argonpass.DefaultMemory,
	Parallelism: argonpass.DefaultParallelism,
	OutputSize:  argonpass.DefaultOutputSize,
	Function:    argonpass.DefaultFunction,
	SaltSize:    argonpass.DefaultSaltSize,
}

func LoadConfig(path string) {
	config.WithOptions(config.ParseEnv)
	// add driver for support yaml content
	config.AddDriver(yaml.Driver)
	err := config.LoadFiles(path)

	if err != nil {
		log.Error("Configuration File Reading Error.", err)
		panic("Configuration File Reading Error.")
	}

	readConfiguration()
}

func readConfiguration() {
	configuration = Configuration{}
	configuration.debug = readDebug()

	configuration.databaseType = readStringValue("databaseType")
	configuration.databaseHost = readStringValue("databaseHost")
	configuration.databaseUsername = readStringValue("databaseUsername")
	configuration.databasePassword = readStringValue("databasePassword")
	configuration.databaseDatabase = readStringValue("databaseDatabase")
	configuration.databasePort = readIntValue("databasePort")
	configuration.defaultResponse = readStringValue("defaultResponse")
	configuration.urlPrefix = readStringValue("urlPrefix")
	configuration.port = readIntValue("port")
	configuration.suffixLength = readIntValue("suffixLength")
	configuration.username = readStringValue("username")
}

func CreateHashIfNotExisting() bool {
	hashed, successful := config.String("passwordHashed")
	if !successful || hashed == "" {
		unhashed, successful2 := config.String("password")
		if !successful2 {
			log.Error("No Valid Password Configured!")
			panic("No Valid Password Configured!")
		}
		updateHashedPassword(configuration.username, unhashed)
		return true
	}
	return false
}

func validatePassword(username, password string) bool {
	if CreateHashIfNotExisting() {
		validatePassword(username, password)
		log.Info("Regenerate Password")
	}
	hashed, _ := config.String("passwordHashed")
	err := argonpass.Verify(getAuthString(username, password), hashed)
	return err == nil
}

func updateHashedPassword(username string, unhashedPassword string) {
	hashed, err := argonpass.Hash(getAuthString(username, unhashedPassword), &argonParams)

	if err != nil {
		log.Error("Problem while hashing", err)
		panic("Problem while hashing")
	}

	_ = config.Set("passwordHashed", hashed)
	_ = config.Set("password", "")
	_ = config.Set("username", "")

	f := getFile()
	_, _ = config.DumpTo(f, config.Yaml)
	closeError := f.Close()
	if closeError != nil {
		log.Error("Problem while closing File", closeError)
		panic("Problem while closing File")
	}
	if argonpass.Verify(getAuthString(username, unhashedPassword), hashed) != nil {
		log.Panic("Newly created pasword already invalide")
	}
}

func getAuthString(username, password string) string {
	return fmt.Sprintf("%v:%v", username, password)
}

func getFile() *os.File {
	f, ferr := os.OpenFile(fileLocation, os.O_WRONLY|os.O_TRUNC|os.O_CREATE, 0600)
	if ferr != nil {
		log.Error("Could not open Config File.", ferr)
		panic("Could not open Config File.")
	}
	return f
}

func readStringValue(key string) string {
	v, successful := config.String(key)
	if configuration.debug {
		log.Debug(fmt.Sprintf("Reading value %v returned %v and %t", key, v, successful))
	}
	if !successful {
		log.Errorf(configReadErrorMessage, key)
		panic(configReadErrorMessage)
	}
	return v
}
func readIntValue(key string) int {
	v, successful := config.Int(key)
	if configuration.debug {
		log.Debug(fmt.Sprintf("Reading value %v returned %v and %t", key, v, successful))
	}
	if !successful {
		log.Errorf(configReadErrorMessage, key)
		panic(configReadErrorMessage)
	}
	return v
}

func readDebug() bool {
	v, successful := config.Bool("debug")
	if !successful {
		log.Errorf(configReadErrorMessage, "debug")
		panic(configReadErrorMessage)
	}
	return v
}
