package internal

import (
	jsonencode "encoding/json"
	"fmt"
	log "github.com/sirupsen/logrus"
	"github.com/valyala/fasthttp"
)

type Auth struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

type AddRequest struct {
	Username string `json:"username"`
	Password string `json:"password"`
	Url      string `json:"url"`
}

type AddResponse struct {
	Url  string `json:"url"`
	Code string `json:"code"`
}

func defaultResponse(ctx *fasthttp.RequestCtx) {
	ctx.SetStatusCode(fasthttp.StatusOK)
	ctx.SetBody([]byte(configuration.defaultResponse))

}

func redirect(ctx *fasthttp.RequestCtx) {

	query := ctx.QueryArgs()

	// Get Location here
	redirectUrl := findRedirectUrl(query.String())

	ctx.SetStatusCode(fasthttp.StatusFound)
	ctx.Response.Header.Set("Location", redirectUrl.Destination)

	// then write more body
	ctx.SetContentType("text/html")
	body := fmt.Sprintf("<html>\n<head>\n<title>Moved</title>\n</head>\n<body>\n<h1>Moved</h1>\n<p>This page has moved to "+
		"<a href=\"%v\">%v</a>.</p>\n</body>\n</html>",
		redirectUrl.Destination, redirectUrl.Destination)
	// then override already written body
	ctx.SetBody([]byte(body))
}

func add(ctx *fasthttp.RequestCtx) {
	var addRequest AddRequest

	err := jsonencode.Unmarshal(ctx.PostBody(), &addRequest)
	if err != nil {
		log.Printf("Not correct Json in Request. Body=%v", err)
		ctx.SetStatusCode(fasthttp.StatusNotFound)
		return
	}
	if addRequest.Url == "" {
		log.Printf("URL was missing in add Request")
		ctx.SetStatusCode(fasthttp.StatusMethodNotAllowed)
		return
	}

	authed := auth(Auth{addRequest.Username, addRequest.Password})
	if !authed {
		ctx.SetStatusCode(fasthttp.StatusNotFound)
		return
	}
	var addResponse = AddResponse{
		Url:  addRequest.Url,
		Code: addUrl(addRequest.Url),
	}
	body, jerror := jsonencode.Marshal(&addResponse)
	if jerror != nil {
		log.Printf("Error in Marshalling Response. AddRepose=%v |error=%v\n", addResponse, err)
	}

	ctx.SetBody(body)

}

func all(ctx *fasthttp.RequestCtx) {
	var authBody Auth
	err := jsonencode.Unmarshal(ctx.PostBody(), &authBody)

	if err != nil {
		log.Printf("Non Json in Reqeust. Body=%d\n", err)
	}

	authed := auth(authBody)
	if !authed {
		ctx.SetStatusCode(fasthttp.StatusNotFound)
		return
	}

	var allResponse = getAll()

	body, jerror := jsonencode.Marshal(&allResponse)
	if jerror != nil {
		log.Printf("Error in Marshalling Response. AllReponse=%v |error=%v\n", allResponse, jerror)
	}

	ctx.SetBody(body)
}

func auth(auth Auth) bool {
	return validatePassword(auth.Username, auth.Password)
}

func requestHandler(ctx *fasthttp.RequestCtx) {
	path := fmt.Sprintf("/%v", configuration.urlPrefix)

	if path == "/add" || path == "/all" {
		log.Fatalf("Prefix can't be \"add\" or \"all\". Configured Prefix was %v", configuration.urlPrefix)
	}

	switch string(ctx.Path()) {
	case path:
		redirect(ctx)
	case "/add":
		add(ctx)
	case "/all":
		all(ctx)
	default:
		defaultResponse(ctx)
	}
}

func StartServer() {
	port := fmt.Sprintf(":%d", configuration.port)
	log.Printf("Starting server under %v, watching for path /%v\n", port, configuration.urlPrefix)
	_ = fasthttp.ListenAndServe(port, requestHandler)
}
