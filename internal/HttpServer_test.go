package internal

import (
	"bufio"
	"context"
	jsonencode "encoding/json"
	"fmt"
	"github.com/gookit/config"
	"github.com/stretchr/testify/assert"
	"github.com/valyala/fasthttp"
	"github.com/valyala/fasthttp/fasthttputil"
	"sync"
	"testing"
)

// Make sure fasthttp.RequestCtx implements context.Context
var _ context.Context = &fasthttp.RequestCtx{}

func TestStatic(t *testing.T) {
	ln := setupWebserver(t)

	request := testRequest{
		method:  "GET",
		path:    "/",
		content: nil,
	}

	resp := sendRequest(t, ln, request)
	assert.Equal(t, fasthttp.StatusOK, resp.StatusCode(), "Should return status code 200 OK")
	stringBody := string(resp.Body())
	assert.Equal(t, configuration.defaultResponse, stringBody, "Should return %v but was %v", configuration.defaultResponse, stringBody)

	cleanupDatabase()
	config.ClearAll()
}
func TestAdd(t *testing.T) {
	ln := setupWebserver(t)

	addRequest := AddRequest{
		Username: "shortadmin",
		Password: "Testinger",
		Url:      "https://gitlab.com/Serverfrog/goshort",
	}

	request := testRequest{
		method:  "GET",
		path:    "/add",
		content: getAddRequestBody(addRequest, t),
	}

	resp := sendRequest(t, ln, request)
	assert.Equal(t, fasthttp.StatusOK, resp.StatusCode(), "Should return status code 200 OK")

	var addResponse AddResponse

	err := jsonencode.Unmarshal(resp.Body(), &addResponse)
	assert.Nil(t, err, "Should not have an Error")

	assert.NotNil(t, addResponse.Code, "Response Code should not be Nil")
	assert.Equal(t, "https://gitlab.com/Serverfrog/goshort", addResponse.Url, "Url should be the same as sended")

	cleanupDatabase()
	config.ClearAll()
}
func TestAddMissingUrl(t *testing.T) {
	ln := setupWebserver(t)

	addRequest := AddRequest{
		Username: "shortadmin",
		Password: "Testinger",
		Url:      "",
	}

	request := testRequest{
		method:  "GET",
		path:    "/add",
		content: getAddRequestBody(addRequest, t),
	}

	resp := sendRequest(t, ln, request)
	assert.Equal(t, fasthttp.StatusMethodNotAllowed, resp.StatusCode(), "Should return status code 405 StatusMethodNotAllowed")

	cleanupDatabase()
	config.ClearAll()
}

func TestAddWrongUser(t *testing.T) {
	ln := setupWebserver(t)

	addRequest := AddRequest{
		Username: "wronguser",
		Password: "Testinger",
		Url:      "https://gitlab.com/Serverfrog/goshort",
	}

	request := testRequest{
		method:  "GET",
		path:    "/add",
		content: getAddRequestBody(addRequest, t),
	}

	resp := sendRequest(t, ln, request)
	assert.Equal(t, fasthttp.StatusNotFound, resp.StatusCode(), "Should return status code 404 Not Found")
	cleanupDatabase()
	config.ClearAll()
}
func TestAddWrongPassword(t *testing.T) {
	ln := setupWebserver(t)

	addRequest := AddRequest{
		Username: "shortadmin",
		Password: "wrongpassword",
		Url:      "https://gitlab.com/Serverfrog/goshort",
	}

	request := testRequest{
		method:  "GET",
		path:    "/add",
		content: getAddRequestBody(addRequest, t),
	}

	resp := sendRequest(t, ln, request)
	assert.Equal(t, fasthttp.StatusNotFound, resp.StatusCode(), "Should return status code 404 Not Found")
	cleanupDatabase()
	config.ClearAll()
}

func TestGetAllAuthed(t *testing.T) {
	ln := setupWebserver(t)

	redirectUrl := addUrl("https://gitlab.com/Serverfrog/goshort")
	assert.NotEmpty(t, redirectUrl, "URL Should not be empty")

	redirectUrl2 := addUrl("https://gitlab.com/Serverfrog/")
	assert.NotEmpty(t, redirectUrl2, "URL Should not be empty")

	auth := Auth{
		Username: "shortadmin",
		Password: "Testinger",
	}

	request := testRequest{
		method:  "GET",
		path:    "/all",
		content: getAuthBody(auth, t),
	}

	resp := sendRequest(t, ln, request)

	assert.Equal(t, fasthttp.StatusOK, resp.StatusCode(), "Should return 200 OK")

	var allResponse []RedirectUrl

	err := jsonencode.Unmarshal(resp.Body(), &allResponse)
	assert.Nil(t, err, "Should not have an Error")

	assert.NotEmpty(t, allResponse, "Response should not be empty")
	assert.Equal(t, 2, len(allResponse), "Should be of size 2")

	assert.Equal(t, allResponse[0].Random, findRandom("https://gitlab.com/Serverfrog/goshort").Random, "Should be same Random")
	assert.Equal(t, allResponse[0].ID, findRandom("https://gitlab.com/Serverfrog/goshort").ID, "Should be same ID")
	assert.Equal(t, allResponse[0].Destination, findRandom("https://gitlab.com/Serverfrog/goshort").Destination, "Should be same Destination")

	assert.Equal(t, allResponse[1].Random, findRandom("https://gitlab.com/Serverfrog/").Random, "Should be same Random")
	assert.Equal(t, allResponse[1].ID, findRandom("https://gitlab.com/Serverfrog/").ID, "Should be same ID")
	assert.Equal(t, allResponse[1].Destination, findRandom("https://gitlab.com/Serverfrog/").Destination, "Should be same Destination")

	cleanupDatabase()
	config.ClearAll()
}

func TestRedirect(t *testing.T) {
	ln := setupWebserver(t)

	redirectUrl := addUrl("https://gitlab.com/Serverfrog/goshort")
	assert.NotEmpty(t, redirectUrl, "URL Should not be empty")

	configuredPath := fmt.Sprintf("/watch?%v", redirectUrl)

	request := testRequest{
		method:  "GET",
		path:    configuredPath,
		content: nil,
	}

	resp := sendRequest(t, ln, request)
	assert.Equal(t, fasthttp.StatusFound, resp.StatusCode(), "Should return status code 302 Found")
	location := resp.Header.Peek("Location")

	assert.Equal(t, "https://gitlab.com/Serverfrog/goshort", string(location), "Location should be same as configured")

	cleanupDatabase()
	config.ClearAll()
}

//Helper Functions

func setupWebserver(t *testing.T) *fasthttputil.InmemoryListener {

	loadTestConfig(inmemory, t)
	readConfiguration()
	Connect()

	s := &fasthttp.Server{
		Handler: requestHandler,
		Logger:  &testLogger{},
	}

	ln := fasthttputil.NewInmemoryListener()

	go func() {
		if err := s.Serve(ln); err != nil {
			t.Errorf("unexpected error: %s", err)
		}
	}()
	return ln
}

func getAddRequestBody(request AddRequest, t *testing.T) []byte {
	body, jerror := jsonencode.Marshal(&request)
	assert.Nil(t, jerror, "Error in Marshalling Response. AllReponse=%v |error=%v\n", request, jerror)
	return body
}
func getAuthBody(request Auth, t *testing.T) []byte {
	body, jerror := jsonencode.Marshal(&request)
	assert.Nil(t, jerror, "Error in Marshalling Response. AllReponse=%v |error=%v\n", request, jerror)
	return body
}

func sendRequest(t *testing.T, ln *fasthttputil.InmemoryListener, request testRequest) fasthttp.Response {
	c, err := ln.Dial()
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}
	if _, err = c.Write(request.bytes()); err != nil {
		t.Fatal(err)
	}

	br := bufio.NewReader(c)
	var resp fasthttp.Response
	if err := resp.Read(br); err != nil {
		t.Fatalf("unexpected error: %s", err)
	}
	//nolint:noinspection GoVetCopyLock
	return resp
}

type testRequest struct {
	method  string
	path    string
	content []byte
}

func (request *testRequest) bytes() []byte {
	return []byte(fmt.Sprintf("%v %v HTTP/1.1\r\nHost: serverfrog.de\r\nContent-Length: %d\n\n%v",
		request.method, request.path, len(request.content), string(request.content)))
}

type testLogger struct {
	*sync.Mutex
	out string
}

func (cl *testLogger) Printf(format string, args ...interface{}) {
	cl.Lock()
	cl.out += fmt.Sprintf(format, args...)[6:] + "\n"
	cl.Unlock()
}

const inmemory = `
databaseType: sqlite
databaseHost: file::memory:?cache=shared
databaseDatabase: database
databaseUsername: username
databasePassword: password
databasePort: 1337
defaultResponse: cumming soon!
password: ""
passwordHashed: $argon2id$v=19$m=65536,t=1,p=4$0aUpjzjWufeSmwqIpuiANQ==$t6XhI00JCRh9CBCi+zcc1w==
port: 80
suffixLength: 10
urlPrefix: watch
username: ""
debug: false`
