package internal

import (
	"github.com/gookit/config"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestConnect(t *testing.T) {
	loadTestConfig(non_debug, t)
	setUpTestDatabase()
	Connect()

	config.ClearAll()
	cleanupDatabase()
}

func TestConnectToNonExistingDatabase(t *testing.T) {
	loadTestConfig(non_debug, t)
	readConfiguration()
	configuration.databaseHost = "/"

	assert.Panics(t, func() {
		Connect()
	}, "Should Panic when try to connect to a non-existing database")
	config.ClearAll()
}

func TestConnectToNonExistingDatabaseServer(t *testing.T) {
	loadTestConfig(non_debugPostgresql, t)
	readConfiguration()
	configuration.databaseHost = "/"

	assert.Panics(t, func() {
		Connect()
	}, "Should Panic when try to connect to a non-existing database")
	config.ClearAll()
}

func TestAddUrl(t *testing.T) {
	loadTestConfig(non_debug, t)
	readConfiguration()
	setUpTestDatabase()
	Connect()

	url := addUrl("https://gitlab.com/Serverfrog/goshort")
	assert.NotEmpty(t, url, "URL Should not be empty")

	cleanupDatabase()
	config.ClearAll()
}
func TestAddUrlExisting(t *testing.T) {
	loadTestConfig(non_debug, t)
	readConfiguration()
	setUpTestDatabase()
	Connect()

	url := addUrl("https://gitlab.com/Serverfrog/goshort")
	assert.NotEmpty(t, url, "URL Should not be empty")

	url2 := addUrl("https://gitlab.com/Serverfrog/goshort")
	assert.NotEmpty(t, url2, "URL Should not be empty")
	assert.Equal(t, url, url2, "URL Should be equal")

	cleanupDatabase()
	config.ClearAll()
}

func TestFindRedirect(t *testing.T) {
	loadTestConfig(non_debug, t)
	readConfiguration()
	setUpTestDatabase()
	Connect()

	url := findRedirectUrl("xxxxxx")
	assert.Empty(t, url, "Should not be filled")

	redirectUrl := addUrl("https://gitlab.com/Serverfrog/goshort")
	assert.NotEmpty(t, redirectUrl, "URL Should not be empty")
	redirectUrl2 := findRedirectUrl(redirectUrl)
	assert.NotEmpty(t, redirectUrl2, "URL Should not be empty")
	assert.Equal(t, redirectUrl, redirectUrl2.Random, "URL Should be equal")
	cleanupDatabase()
	config.ClearAll()
}

func TestFindNothing(t *testing.T) {
	loadTestConfig(non_debug, t)
	readConfiguration()
	setUpTestDatabase()
	Connect()

	url := findRedirectUrl("")

	assert.Equal(t, uint(0), url.ID, "ID should be 0")

	cleanupDatabase()
	config.ClearAll()
}

func TestGetAll(t *testing.T) {
	loadTestConfig(non_debug, t)
	readConfiguration()
	setUpTestDatabase()
	Connect()

	emptyAll := getAll()
	assert.Empty(t, emptyAll, "Should not be filled")
	//addUrl Returns a String, we need a RedirectUrl for the comparision
	redirectUrl := findRedirectUrl(addUrl("https://gitlab.com/Serverfrog/goshort"))
	redirectUrl2 := findRedirectUrl(addUrl("https://gitlab.com/Serverfrog"))

	filled := getAll()
	assert.Contains(t, filled, redirectUrl, "Should Container redirectUrl")
	assert.Contains(t, filled, redirectUrl2, "Should Container redirectUrl2")

	cleanupDatabase()
	config.ClearAll()
}

//Helper Functions
func cleanupDatabase() {
	database.Exec("DELETE FROM redirect_urls")
	sqlDB, err1 := database.DB()
	if err1 != nil {
		panic(err1)
	}
	_ = sqlDB.Close()

	database = nil
}

func setUpTestDatabase() {
	configuration.databaseHost = "file::memory:?cache=shared"
}
