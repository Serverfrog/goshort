package internal

import (
	"fmt"
	"github.com/gookit/config"
	"github.com/gookit/config/yaml"
	"github.com/stretchr/testify/assert"
	"os"
	"testing"
)

func BenchmarkReadConfig(b *testing.B) {
	if configuration.username == "" {
		loadTestConfigBenchmark(debug, b)
	}
	for n := 0; n < b.N; n++ {
		readConfiguration()
	}
}

func BenchmarkValidValidation(b *testing.B) {
	if configuration.username == "" {
		loadTestConfigBenchmark(debug, b)
		readConfiguration()

		_ = config.Set("passwordHashed", "")
		_ = config.Set("password", "Testinger")
		_ = config.Set("username", "shortadmin")
	}
	for n := 0; n < b.N; n++ {
		b2 := validatePassword("shortadmin", "Testinger")
		if !b2 {
			b.Fatalf("Not Valid anymore")
		}
		_ = config.Set("passwordHashed", "")
		_ = config.Set("password", "Testinger")
		_ = config.Set("username", "shortadmin")
	}
}
func BenchmarkUnvalidValidation(b *testing.B) {
	if configuration.username == "" {
		loadTestConfigBenchmark(debug, b)
		readConfiguration()

		_ = config.Set("passwordHashed", "")
		_ = config.Set("password", "xxxxxx")
		_ = config.Set("username", "xxxxxx")
	}
	for n := 0; n < b.N; n++ {
		b2 := validatePassword("YYYYYYY", "YYYYYYY")
		if b2 {
			b.Fatalf("Suddenly Valid")
		}
		_ = config.Set("passwordHashed", "")
		_ = config.Set("password", "xxxxxx")
		_ = config.Set("username", "xxxxxx")
	}
}

func TestLoadConfig(t *testing.T) {
	LoadConfig("../config.yml")
	assert.NotEmpty(t, configuration, "Configuration should not be Empty")
	config.ClearAll()
}
func TestLoadConfigWrongfile(t *testing.T) {
	assert.Panics(t, func() {
		LoadConfig("doesNotExist.yml")
	}, "Should Panic with Wrong File")
	config.ClearAll()
}

func TestReadConfiguration(t *testing.T) {
	loadTestConfig(non_debug, t)
	readConfiguration()
	assert.Equal(t, "test.db", configuration.databaseHost, "Value should be configured Correctly. databaseHost")
	assert.Equal(t, "cummming soon!", configuration.defaultResponse, "Value should be configured Correctly. defaultResponse")
	assert.Equal(t, 80, configuration.port, "Value should be configured Correctly. port")
	assert.Equal(t, 10, configuration.suffixLength, "Value should be configured Correctly. suffixLength")
	assert.Equal(t, "watch", configuration.urlPrefix, "Value should be configured Correctly. urlPrefix")
	assert.Equal(t, "shortadmin", configuration.username, "Value should be configured Correctly. username")
	assert.Equal(t, false, configuration.debug, "Value should be configured Correctly. debug")

	config.ClearAll()
}

func TestReadConfigurationDebug(t *testing.T) {
	loadTestConfig(debug, t)
	readConfiguration()
	assert.Equal(t, "test.db", configuration.databaseHost, "Value should be configured Correctly. databaseHost")
	assert.Equal(t, "cummming soon!", configuration.defaultResponse, "Value should be configured Correctly. defaultResponse")
	assert.Equal(t, 80, configuration.port, "Value should be configured Correctly. port")
	assert.Equal(t, 10, configuration.suffixLength, "Value should be configured Correctly. suffixLength")
	assert.Equal(t, "watch", configuration.urlPrefix, "Value should be configured Correctly. urlPrefix")
	assert.Equal(t, "shortadmin", configuration.username, "Value should be configured Correctly. username")
	assert.Equal(t, true, configuration.debug, "Value should be configured Correctly. debug")

	config.ClearAll()
}

func TestReadConfigurationPassword(t *testing.T) {
	loadTestConfig(password, t)
	readConfiguration()
	assert.Equal(t, "test.db", configuration.databaseHost, "Value should be configured Correctly. databaseHost")
	assert.Equal(t, "cummming soon!", configuration.defaultResponse, "Value should be configured Correctly. defaultResponse")
	assert.Equal(t, 80, configuration.port, "Value should be configured Correctly. port")
	assert.Equal(t, 10, configuration.suffixLength, "Value should be configured Correctly. suffixLength")
	assert.Equal(t, "watch", configuration.urlPrefix, "Value should be configured Correctly. urlPrefix")
	assert.Equal(t, "shortadmin", configuration.username, "Value should be configured Correctly. username")
	assert.Equal(t, false, configuration.debug, "Value should be configured Correctly. debug")

	config.ClearAll()
}

func TestNonExistingKeyString(t *testing.T) {
	loadTestConfig(non_debug, t)
	assert.Panics(t, func() {
		readStringValue("non-existing")
	}, "Should Panic with non-existing Key for StringValue")
	config.ClearAll()
}

func TestNonExistingKeyInt(t *testing.T) {
	loadTestConfig(non_debug, t)
	assert.Panics(t, func() {
		readIntValue("non-existing")
	}, "Should Panic with non-existing Key for StringValue")
	config.ClearAll()
}

func TestNonExistingKeyDebug(t *testing.T) {
	loadTestConfig("non: existing", t)
	assert.Panics(t, func() {
		readDebug()
	}, "Should Panic with non-existing Key for StringValue")
	config.ClearAll()
}

func TestPasswordUpdate(t *testing.T) {
	dir := t.TempDir()
	fileLocation = fmt.Sprintf("%v/%v", dir, "config.yml")
	loadTestConfig(non_debug, t)
	readConfiguration()
	nonHashed := readStringValue("password")
	assert.Equal(t, "Testinger", nonHashed, "Non Hashed Password should be Testinger")
	updateHashedPassword("shortadmin", "Testinger")

	newNonHashed := readStringValue("password")
	hashed := readStringValue("passwordHashed")
	assert.Equal(t, "", newNonHashed, "Non Hashed Password should be Empty")
	assert.NotEmpty(t, hashed, "Hashed Password should not be Empty")

	config.ClearAll()
	err := os.RemoveAll(dir)
	if err != nil {
		panic(err)
	}
}

func TestPasswordToShort(t *testing.T) {
	dir := t.TempDir()
	fileLocation = fmt.Sprintf("%v/%v", dir, "config.yml")
	loadTestConfig(toShortPassword, t)
	readConfiguration()
	nonHashed := readStringValue("password")
	assert.Equal(t, "short", nonHashed, "Non Hashed Password should be short")

	assert.Panics(t, func() {
		updateHashedPassword("a", "b")
	}, "Should Panic with to password which is to short")

	config.ClearAll()
	err := os.RemoveAll(dir)
	if err != nil {
		panic(err)
	}
}
func TestPasswordFileNotWriteAble(t *testing.T) {
	fileLocation = "/"
	loadTestConfig(non_debug, t)
	readConfiguration()

	assert.Panics(t, func() {
		updateHashedPassword("shortadmin", "Testinger")
	}, "Should Panic with a non-existing File")

	config.ClearAll()
}

func TestValidatePassword(t *testing.T) {
	dir := t.TempDir()
	fileLocation = fmt.Sprintf("%v/%v", dir, "config.yml")
	loadTestConfig(password, t)
	readConfiguration()
	valid := validatePassword("shortadmin", "Testinger")
	assert.True(t, valid, "Should be a Valid Password")

	config.ClearAll()
	err := os.RemoveAll(dir)
	if err != nil {
		panic(err)
	}
}
func TestValidatePasswordWithoutHashed(t *testing.T) {
	dir := t.TempDir()
	fileLocation = fmt.Sprintf("%v/%v", dir, "config.yml")
	loadTestConfig(non_debug, t)
	readConfiguration()

	nonHashed := readStringValue("password")
	assert.Equal(t, "Testinger", nonHashed, "Non Hashed Password should be Testinger")
	CreateHashIfNotExisting()

	valid := validatePassword("shortadmin", "Testinger")
	assert.True(t, valid, "Should be a Valid Password")

	config.ClearAll()
	err := os.RemoveAll(dir)
	if err != nil {
		panic(err)
	}
}

func TestValidatePasswordWithoutPassword(t *testing.T) {
	dir := t.TempDir()
	fileLocation = fmt.Sprintf("%v/%v", dir, "config.yml")
	loadTestConfig(noPassword, t)
	readConfiguration()

	assert.Panics(t, func() {
		validatePassword("shortadmin", "non-existing-password")
	}, "Should Panic with a non-existing Password Property")

	config.ClearAll()
	err := os.RemoveAll(dir)
	if err != nil {
		panic(err)
	}
}

func loadTestConfig(configString string, t *testing.T) {
	config.WithOptions(config.ParseEnv)
	// add driver for support yaml content
	config.AddDriver(yaml.Driver)
	err := config.LoadStrings(config.Yaml, configString)
	assert.Nil(t, err, "Should have no Problem Loading Dummy Data")
}

func loadTestConfigBenchmark(configString string, b *testing.B) {
	config.WithOptions(config.ParseEnv)
	// add driver for support yaml content
	config.AddDriver(yaml.Driver)
	err := config.LoadStrings(config.Yaml, configString)
	assert.Nil(b, err, "Should have no Problem Loading Dummy Data")
}

const non_debug = `
databaseType: sqlite
databaseHost: test.db
databaseDatabase: database
databaseUsername: username
databasePassword: password
databasePort: 1337
defaultResponse: cummming soon!
password: Testinger
port: 80
suffixLength: 10
urlPrefix: watch
username: shortadmin
debug: false`
const debug = `
databaseType: sqlite
databaseHost: test.db
databaseDatabase: database
databaseUsername: username
databasePassword: password
databasePort: 1337
defaultResponse: cummming soon!
password: Testinger
port: 80
suffixLength: 10
urlPrefix: watch
username: shortadmin
debug: true`
const password = `
databaseType: sqlite
databaseHost: test.db
databaseDatabase: database
databaseUsername: username
databasePassword: password
databasePort: 1337
defaultResponse: cummming soon!
password: ""
passwordHashed: $argon2id$v=19$m=65536,t=2,p=4$YRXX1NrjeghTQ7F2rhwpbQ==$We6PQ+pmkk0AdPeEdF5sEA==
port: 80
suffixLength: 10
urlPrefix: watch
username: shortadmin
debug: false`
const toShortPassword = `
databaseType: sqlite
databaseHost: test.db
databaseDatabase: database
databaseUsername: username
databasePassword: password
databasePort: 1337
defaultResponse: cummming soon!
password: short
port: 80
suffixLength: 10
urlPrefix: watch
username: shortadmin
debug: false`
const noPassword = `
databaseType: sqlite
databaseHost: test.db
databaseDatabase: database
databaseUsername: username
databasePassword: password
databasePort: 1337
defaultResponse: cummming soon!
port: 80
suffixLength: 10
urlPrefix: watch
username: ""
debug: false`
const non_debugPostgresql = `
databaseType: postgresql
databaseHost: test.db
databaseDatabase: database
databaseUsername: username
databasePassword: password
databasePort: 1337
defaultResponse: cummming soon!
password: Testinger
port: 80
suffixLength: 10
urlPrefix: watch
username: shortadmin
debug: false`
