FROM gcr.io/distroless/base-debian11

ADD goShort /

CMD ["/goShort", "-config=/config/config.yml"]
