# goShort
Golang Based URL Shortening based on [fasthttp](https://github.com/valyala/fasthttp).

[![coverage report](https://gitlab.com/Serverfrog/goshort/badges/master/coverage.svg)](https://gitlab.com/Serverfrog/goshort/-/commits/master)
[![pipeline status](https://gitlab.com/Serverfrog/goshort/badges/master/pipeline.svg)](https://gitlab.com/Serverfrog/goshort/-/commits/master)
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=Serverfrog_goshort&metric=alert_status)](https://sonarcloud.io/dashboard?id=Serverfrog_goshort)

## API

### `/`
Returns the configured static Response

### `/add`
Adds a new URL Redirect to it.
Needs`username`, `password` and `url` parameter

`url` is the URL which should be shortened

`username` and `password` is for authentication.

Returns 404 with wrong Authentication
Returns 405 when `url` was missing
Returns 200 with JSON Response when Successful

Request Example:

```
POST /add HTTP/1.1
Host: gitlab.com
Content-Length 74

{"username":"shortadmin","password":"Testinger","url":"https://google.com"}
```

Response Example
`{"url":"http://google.com","code":"XySvcF"}`


### `/all`
Get all configured shortened URLs
Needs`username` and `password` parameter

`url` is the URL which should be shortened

`username` and `password` is for authentication.

Returns 404 with wrong Authentication
Returns 200 with JSON Response when Successful

Request Example:

```
POST /add HTTP/1.1
Host: gitlab.com
Content-Length 74

{"username":"shortadmin","password":"Testinger"}
```


Response Example
```
[
{"ID":1,"CreatedAt":"2021-03-30T00:52:07.5711133+02:00","UpdatedAt":"2021-03-30T00:52:07.5711133+02:00","DeletedAt":null,"Random":"vRpPtxHcO4","Destination":"https://google.de"},
{"ID":2,"CreatedAt":"2021-03-30T18:51:56.7786464+02:00","UpdatedAt":"2021-03-30T18:51:56.7786464+02:00","DeletedAt":null,"Random":"r1ySEhVXqx","Destination":"https://google.com"}
]
```


### `/XXX?YYY`
XXX is then the Configured Prefix for redirecting and YYY is then the Code for redirecting
It will look up if the Code is existing and if it is, it will one redirect via HTTP Status 302 (Found) to that location


## Configuration

| Value           	 | Default        	 | description                                                                                        	                                             |
|-------------------|------------------|--------------------------------------------------------------------------------------------------------------------------------------------------|
| databaseType    	 | sqlite     	     | Type of the Database. Either postgresql or sqlite. Its sqlite on default                                                                       	 |
| databaseHost    	 | ""               | Hostname of the Database Server or on sqlite the Host-path.                                                                        	             |
| databaseDatabase  | ""               | the name of the Database. Ignored for sqlite.                                                                        	                           |
| databaseUsername  | ""               | username for Database. Authentication Ignored for sqlite                                                                        	                |
| databasePassword  | ""               | password for Database. Authentication Ignored for sqlite                                                                        	                |
| databasePort    	 | 0                | port of the Database. Server Ignored for sqlite                                                                        	                         |
| debug           	 | false          	 | enable Debug Logging. Ignored for sqlite                                                                               	                         |
| defaultResponse 	 | cummming soon! 	 | Response sent when accessing `/`                                                                 	                                               |
| password        	 | ""             	 | clear text Password. This will replaced with "" after first Request which needs authentication     	                                             |
| passwordHashed  	 | ""             	 | Argon2 Hashed                                                                                      	                                             |
| port            	 | 80             	 | Port under which this Application is reachable                                                      	                                            |
| suffixLength    	 | 10             	 | length of the Random generated Suffix                                                              	                                             |
| urlPrefix       	 | watch          	 | First part of the URL for redirection. Example here `watch?XXXXX` where XXXXX is the Random Suffix 	                                             |
| username        	 | shortadmin     	 | username for authentication                                                                        	                                             |